# Overview
First pass at containerizing PyTM (https://github.com/izar/pytm).

# Tooling
This container includes:
* PyTM installed via pip (and dependencies)
* `dot` (for generating data flow diagrams)
* `java` and `plantuml.jar` (for generating sequence diagrams)
* `pandoc` (for generating reports)

# Convenience Scripts
## Build
0) Install Docker, clone this repo.
1) Build this container. For convenience, `cd` to this repo and run `./build.sh`, and you'll get a `pytm:latest` container on your Docker host.

## Run
1) Write a custom .py file describing your system for PyTM, saving it as `pytm-input/custom-tm.py`.
2) Execute `./run.sh`, and you'll get a shell in the container with bind-mounts for PytM input (`~/pytm-input`) and output (`~/generated`).
3) In the container's `~` (which `run.sh` executed from the host places you in by default), there are convenience scripts (`generate-*.sh`) for generating a DFD, sequence diagram, and report. Run whichever of these you like (or multiple), then exit the container.

## Script Assumptions
* The custom .py file (`pytm-input/custom-tm.py` on the host) is available via bind-mount in-container (at `/home/pytm/pytm-input/custom-tm.py`).
* `./generated` is available via bind-mount in-container (at `/home/pytm/generated`). This directory is used to store generated diagrams/reports.

# Manual Usage
The convenience scripts are probably fastest. If you prefer to do things manually:
1) Run the container with a bind-mount to get your custom `tm.py` into the container. Use another bind-mount to save the generated artifacts somewhere on the container host's filesystem.
2) Run your custom .py file and associated tooling/piping in-container, then saved to the generated artifacts to a bind-mount.
