#!/usr/bin/env bash

# See README.md's script assumptions
python3 $HOME/pytm-input/custom-tm.py --report $HOME/pytm-repo/docs/template.md | pandoc -f markdown -t html > $HOME/generated/custom-tm-report.html
