#!/usr/bin/env bash

# See README.md's script assumptions
python3 $HOME/pytm-input/custom-tm.py --seq | java -Djava.awt.headless=true -jar $HOME/plantuml.jar -tpng -pipe > $HOME/generated/custom-tm-seq.png
