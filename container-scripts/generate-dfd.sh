#!/usr/bin/env bash

# See README.md's script assumptions
python3 $HOME/pytm-input/custom-tm.py --dfd | dot -Tpng -o $HOME/generated/custom-tm-dfd.png
