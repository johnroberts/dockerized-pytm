FROM debian:latest

# Install dependencies as root
RUN apt-get update && apt-get install -y --no-install-recommends \
  python3 python3-pip git graphviz openjdk-11-jre-headless pandoc

# Created an unprivileged user
RUN addgroup --gid 1000 pytm && \
    adduser --uid 1000 --gid 1000 --shell /bin/bash pytm

# Drop to unprivileged user
USER pytm:pytm

# Install PyTM via pip3
RUN pip3 install pytm

# Clone repo, for access via shell and for docs/template.md
RUN git clone https://github.com/izar/pytm.git $HOME/pytm-repo

# Copy plantuml.jar into $HOME for convenience
ADD plantuml.jar /home/pytm/

# Copy scripts into $HOME for convenience
ADD container-scripts/*.sh /home/pytm/

# Set workdir for a nicer shell by default
WORKDIR /home/pytm

# Shell by default, since PyTM relies on piping stuff around
CMD ["bin/bash"]
